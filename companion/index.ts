import { me } from 'companion';
import { app } from 'peer';
import { SettingsListener } from './SettingsListener';
import { SettingsRepository } from './SettingsRepository';

const settingsListener = new SettingsListener();
settingsListener.subscribe();

if (me.launchReasons.settingsChanged) {
    SettingsRepository.setToggleSetting('synched', false);
}

app.addEventListener('readystatechange', () => {
    if (app.readyState !== 'started') {
        return;
    }

    const synched = SettingsRepository.getToggleSetting('synched', false);
    if (synched === true) {
        return;
    }

    settingsListener.sendSettingsToWatch();
});
