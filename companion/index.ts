import { encode } from 'cbor';
import { outbox } from 'file-transfer';
import { settingsStorage } from 'settings';
import { SettingsKeysType } from '../settings';
import { SettingsRepository } from './SettingsRepository';

let outboxTimer: number;

const settings = {
    highValue: SettingsRepository.getSliderSetting('highValue', 100) / 100.0,
    medValue: SettingsRepository.getSliderSetting('medValue', 50) / 100.0,
    lowValue: SettingsRepository.getSliderSetting('lowValue', 0) / 100.0,
    starting: parseInt(SettingsRepository.getSelectSingleSetting('starting', '2'), 10),
    interval: parseInt(SettingsRepository.getTextInputSetting('interval', '30'), 10) * 1000,
    defaultRed: SettingsRepository.getToggleSetting('defaultRed', false),
};

settingsStorage.addEventListener('change', settingsChangeHandler);

async function sendSettingsToWatch() {
    try {
        await outbox.enqueue('settings.cbor', encode(settings));
        console.log(`settings sent: ${JSON.stringify(settings)}`);
    } catch (ex) {
        console.log(`Error sending settings: ${JSON.stringify(ex)}`);
    }
}

function settingsChangeHandler(event: StorageChangeEvent) {
    let value: any;

    switch (event.key) {
    case 'highValue':
    case 'medValue':
    case 'lowValue':
        value = SettingsRepository.getSliderSetting(event.key) / 100.0;
        break;

    case 'starting':
        value = parseInt(SettingsRepository.getSelectSingleSetting(event.key), 10);
        break;

    case 'defaultRed':
        value = SettingsRepository.getToggleSetting(event.key);
        break;

    case 'interval':
        value = parseInt(SettingsRepository.getTextInputSetting(event.key), 10) * 1000;

        if (value < 1) {
            SettingsRepository.setTextInputSetting(event.key, '1');
            value = 1000;
        }

        break;

    default:
        return;
    }

    settings[event.key as SettingsKeysType] = value;
    clearTimeout(outboxTimer);
    outboxTimer = setTimeout(sendSettingsToWatch, 500);
}
