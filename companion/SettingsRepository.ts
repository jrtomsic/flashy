import { settingsStorage } from 'settings';

export class SettingsRepository {
    private static getSetting(name: string): string {
        const item = settingsStorage.getItem(name);

        if (item === null || item === undefined) {
            console.log(`'${name}' doesn't exist in settingsStorage`);
            return null;
        }

        return item;
    }

    // { name: 'value' }
    public static getTextInputSetting(name: string, defaultValue: string = null): string {
        const item = this.getSetting(name);

        if (item === null) {
            return defaultValue;
        }

        let value = defaultValue;
        try {
            value = JSON.parse(item).name;
        } catch (ex) {
            console.log(`error parsing json from settingsStorage '${name}': ${item}`);
            return defaultValue;
        }

        if (value === '') {
            console.log(`value for '${name}' is blank in settingsStorage`);
            return defaultValue;
        }

        return value;
    }

    public static setTextInputSetting(name: string, value: string): void {
        settingsStorage.setItem(name, JSON.stringify({ name: value }));
    }

    public static getSliderSetting(name: string, defaultValue: number = null): number {
        const item = this.getSetting(name);

        if (item === null) {
            return defaultValue;
        }

        return parseFloat(item);
    }

    public static setToggleSetting(name: string, value: boolean): void {
        if (value === true) {
            settingsStorage.setItem(name, 'true');
            return;
        }

        settingsStorage.setItem(name, 'false');
    }

    public static getToggleSetting(name: string, defaultValue: boolean = null): boolean {
        const item = this.getSetting(name);

        if (item === null) {
            return defaultValue;
        }

        return (item === 'true');
    }

    // { values: [{ name: 'high', value: '2' }], selected: [0] }
    public static getSelectSingleSetting(name: string, defaultValue: string = null): string {
        const item = this.getSetting(name);

        if (item === null) {
            return defaultValue;
        }

        let value = defaultValue;
        try {
            value = JSON.parse(item).values[0].value;
        } catch (ex) {
            console.log(`error parsing json from settingsStorage '${name}': ${item}`);
            return defaultValue;
        }

        return value;
    }
}
