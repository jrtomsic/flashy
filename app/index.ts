import { ButtonListener } from './ButtonListener';
import { InboxListener } from './InboxListener';
import { SettingsHandler } from './SettingsHandler';
import { UnloadListener } from './UnloadListener';
import { VibrationHandler } from './VibrationHandler';

const vibrationHandler = new VibrationHandler();
const settingsHandler = new SettingsHandler(vibrationHandler);
const unloadListener = new UnloadListener(vibrationHandler);
const buttonListener = new ButtonListener(settingsHandler);
const inboxListener = new InboxListener(settingsHandler);

unloadListener.subscribe();
buttonListener.subscribe();
inboxListener.subscribe();

settingsHandler.updateSettings();
