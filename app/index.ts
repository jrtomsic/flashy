import { me } from 'appbit';
import { display } from 'display';
import document from 'document';
import { inbox } from 'file-transfer';
import { readFileSync } from 'fs';
import { vibration } from 'haptics';

let brightnessValues: number[];
let currentBrightness: number;
let vibrationTimer: number;
let brightnessTimer: number;

// TODO change to a more noticeable vibration pattern

me.addEventListener('unload', unloadHandler);
document.addEventListener('keypress', buttonHandler);
display.addEventListener('change', displayHandler);
inbox.addEventListener('newfile', inboxHandler);
applySettings();

function applySettings() {
    const settings = readSettings();

    brightnessValues = [
        settings.lowValue,
        settings.medValue,
        settings.highValue,
    ];

    currentBrightness = settings.starting;
    display.brightnessOverride = brightnessValues[settings.starting];
    display.autoOff = false;

    setColors(settings.defaultRed);

    clearInterval(vibrationTimer);
    vibrationTimer = setInterval(() => { vibration.start('confirmation-max'); }, settings.interval);
}

function setColors(defaultRed: boolean) {
    const defaultPage = document.getElementById('default') as GraphicsElement;
    const secondaryPage = document.getElementById('secondary') as GraphicsElement;

    if (defaultRed === true) {
        defaultPage.style.fill = 'red';
        secondaryPage.style.fill = 'white';
        return;
    }

    defaultPage.style.fill = 'white';
    secondaryPage.style.fill = 'red';
}

function readSettings() {
    let settings;

    try {
        settings = readFileSync('settings.cbor', 'cbor');
        console.log(`found settings: ${JSON.stringify(settings)}`);
    } catch (ex) {
        settings = {
            highValue: 1.0,
            medValue: 0.5,
            lowValue: 0.0,
            interval: 30000,
            starting: 2,
            defaultRed: false,
        };
        console.log('error reading settings, using defaults');
    }

    return settings;
}

function inboxHandler() {
    let fileName;

    while (fileName = inbox.nextFile()) {
        console.log(`File received: ${fileName}`);

        if (fileName === 'settings.cbor') {
            applySettings();
        }
    }
}

function displayHandler() {
    if (display.on === false) {
        me.exit();
    }
}

function buttonHandler(event: KeyboardEvent) {
    console.log(event.key);

    if (event.key === 'up' && currentBrightness < brightnessValues.length - 1) {
        currentBrightness += 1;
        console.log(`to ${currentBrightness}`);
        display.brightnessOverride = brightnessValues[currentBrightness];
    }

    if (event.key === 'down' && currentBrightness > 0) {
        currentBrightness -= 1;
        console.log(`to ${currentBrightness}`);
        display.brightnessOverride = brightnessValues[currentBrightness];
    }

    displayBrightness();
}

function displayBrightness() {
    const brightnessLabels = ['low', 'medium', 'high'];
    const brightness = document.getElementById(`brightness`) as GraphicsElement;

    brightness.text = brightnessLabels[currentBrightness];
    clearTimeout(brightnessTimer);
    brightnessTimer = setTimeout(() => { brightness.text = ''; }, 2000);
}

function unloadHandler() {
    display.brightnessOverride = undefined;
    display.autoOff = true;
    clearInterval(vibrationTimer);
}
