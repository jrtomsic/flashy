import { vibration } from 'haptics';

export class VibrationHandler {
    private tracker: number;
    private time: number;

    public constructor() {
        this.time = 30000;
    }

    public setTime(time: number) {
        if (time < 1000) {
            this.time = 1000;
            return;
        }

        this.time = time;
    }

    public restart() {
        this.stop();
        this.start();
    }

    public start() {
        this.tracker = setInterval(
            this.vibrate,
            this.time,
        );
    }

    public stop() {
        clearInterval(this.tracker);
    }

    private vibrate() {
        vibration.start('nudge-max');
    }
}
