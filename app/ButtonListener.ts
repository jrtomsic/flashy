import document from 'document';
import { SettingsHandler } from './SettingsHandler';

export class ButtonListener {
    private brightnessTimeout: number;

    constructor (private readonly settingsHandler: SettingsHandler) {}

    public subscribe() {
        document.addEventListener('keypress', this.buttonHandler.bind(this));
    }

    private buttonHandler(event: KeyboardEvent) {
        if (event.key === 'up') {
            this.settingsHandler.increaseBrightness();
        }

        if (event.key === 'down') {
            this.settingsHandler.decreaseBrightness();
        }

        this.displayBrightness();
    }

    private displayBrightness() {
        const brightnessLabels = ['low', 'medium', 'high'];
        const brightness = document.getElementById(`brightness`) as GraphicsElement;

        brightness.text = brightnessLabels[this.settingsHandler.getCurrentBrightness()];

        clearTimeout(this.brightnessTimeout);
        this.brightnessTimeout = setTimeout(
            () => { brightness.text = ''; },
            2000,
        );
    }
}
