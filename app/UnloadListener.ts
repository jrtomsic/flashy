import { me } from 'appbit';
import { display } from 'display';
import { VibrationHandler } from './VibrationHandler';

export class UnloadListener {
    constructor (private readonly vibrationHandler: VibrationHandler) {}

    public subscribe() {
        me.addEventListener('unload', this.unloadHandler.bind(this));
        display.addEventListener('change', this.displayHandler.bind(this));
    }

    private unloadHandler() {
        display.brightnessOverride = undefined;
        display.autoOff = true;
        this.vibrationHandler.stop();
    }

    private displayHandler() {
        if (display.on === false) {
            me.exit();
        }
    }
}
